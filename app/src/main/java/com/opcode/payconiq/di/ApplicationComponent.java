package com.opcode.payconiq.di;

import com.opcode.payconiq.view.MainActivity;
import javax.inject.Singleton;
import dagger.Component;

/**
 * Created by TCOERDOGAN on 20.05.2018.
 */
@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {
    void inject(MainActivity mainActivity);
}
