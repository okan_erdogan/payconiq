package com.opcode.payconiq.di;

import android.content.Context;
import com.opcode.payconiq.PayconiqApplication;
import com.opcode.payconiq.presenter.DbPresenter;
import com.opcode.payconiq.presenter.DbProvider;
import com.opcode.payconiq.presenter.JWPresenter;
import com.opcode.payconiq.presenter.JWProvider;
import com.opcode.payconiq.presenter.UtilPresenter;
import com.opcode.payconiq.presenter.UtilProvider;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

/**
 * Created by TCOERDOGAN on 20.05.2018.
 */
@Module
public class ApplicationModule {
    private final PayconiqApplication application;
    public ApplicationModule(PayconiqApplication application) {
        this.application = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return application;
    }

    @Provides
    @Singleton
    JWPresenter provideJWPresenter() {
        return new JWProvider();

    }

    @Provides
    @Singleton
    DbPresenter provideDbPresenter() {
        return new DbProvider(this.application);
    }

    @Provides
    @Singleton
    UtilPresenter provideUtilPresenter() {
        return new UtilProvider();
    }
}
