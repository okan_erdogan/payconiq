package com.opcode.payconiq.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.opcode.payconiq.R;

/**
 * Created by TCOERDOGAN on 20.05.2018.
 */

public class ItemViewHolder extends RecyclerView.ViewHolder {
    public TextView repoName;
    public ItemViewHolder(View itemView) {
        super(itemView);
        this.repoName=itemView.findViewById(R.id.repoName);
    }
}
