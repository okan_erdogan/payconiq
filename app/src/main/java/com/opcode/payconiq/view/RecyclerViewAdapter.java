package com.opcode.payconiq.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.opcode.payconiq.R;
import com.opcode.payconiq.model.PayconiqModel;
import com.opcode.payconiq.model.dao.PayconiqDao;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by TCOERDOGAN on 20.05.2018.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PayconiqModel> payconiqModels=new ArrayList<>();
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view=inflater.inflate(R.layout.item_recycler_view,parent,false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PayconiqModel payconiqModel=payconiqModels.get(position);
        final ItemViewHolder itemViewHolder = (ItemViewHolder) holder;
        itemViewHolder.repoName.setText(position+" - "+payconiqModel.getName());
    }

    public List<PayconiqModel> getPayconiqModels() {
        return payconiqModels;
    }

    public void setPayconiqModels(List<PayconiqDao> payconiqDaos) {
        this.payconiqModels.clear();
        for(PayconiqDao payconiqDao:payconiqDaos){
            this.payconiqModels.add(toPayconiqModel(payconiqDao));
        }
        this.payconiqModels = payconiqModels;
    }

    private PayconiqModel toPayconiqModel(PayconiqDao payconiqDao){
        PayconiqModel payconiqModel=new PayconiqModel();
        payconiqModel.setName(payconiqDao.getName());
        return payconiqModel;
    }

    @Override
    public int getItemCount() {
        return payconiqModels == null ? 0 : payconiqModels.size();
    }


}
