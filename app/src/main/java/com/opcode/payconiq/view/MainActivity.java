package com.opcode.payconiq.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.opcode.payconiq.PayconiqApplication;
import com.opcode.payconiq.R;
import com.opcode.payconiq.model.PayconiqModel;
import com.opcode.payconiq.model.dao.PayconiqDao;
import com.opcode.payconiq.presenter.DbPresenter;
import com.opcode.payconiq.presenter.JWPresenter;
import java.io.IOException;
import java.util.List;
import javax.inject.Inject;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {


    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private ImageView noDataFoundImage;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerViewAdapter recyclerViewAdapter;
    private static final int ITEM_COUNT=15;
    private static final int PAGE_COUNT=1;
    private boolean isResponseArrived=false;
    private int previewSize=0;

    @Inject
    JWPresenter jwPresenter;
    @Inject
    DbPresenter dbPresenter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.recyclerView=findViewById(R.id.recycler_view);
        this.progressBar=findViewById(R.id.progressBar);
        this.noDataFoundImage=findViewById(R.id.noDataFoundImage);
        this.linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        this.recyclerView.setLayoutManager(linearLayoutManager);
        this.recyclerView.setItemAnimator(new DefaultItemAnimator());
        this.recyclerViewAdapter=new RecyclerViewAdapter();
        this.recyclerView.setAdapter(recyclerViewAdapter);

        ((PayconiqApplication)this.getApplication()).getApplicationComponent().inject(this);

        this.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int totalItemCount=linearLayoutManager.getItemCount();
                int diff=totalItemCount-linearLayoutManager.findLastVisibleItemPosition();
                if(diff<4&&dy>0){
                    if(isResponseArrived){
                        isResponseArrived=false;
                        getRepos(jwPresenter,PAGE_COUNT,getRequestPage()*ITEM_COUNT);
                    }
                }
            }
        });


        this.getRepos(jwPresenter,PAGE_COUNT,ITEM_COUNT);
    }


    private void getRepos(JWPresenter jwPresenter,int page,int itemCount){
        progressBar.setVisibility(View.VISIBLE);
        previewSize=linearLayoutManager.getItemCount();
        jwPresenter.getRepos(new Callback<List<PayconiqModel>>() {
            @Override
            public void onResponse(Call<List<PayconiqModel>> call, Response<List<PayconiqModel>> response) {
                progressBar.setVisibility(View.INVISIBLE);
                isResponseArrived=true;
                if(response.body()!=null){
                    for(PayconiqModel payconiqModel:response.body()){
                        dbPresenter.add(toPayconiqDao(payconiqModel));
                    }

                }else{
                    try {
                        Toast.makeText(MainActivity.this,response.errorBody().string(),Toast.LENGTH_LONG).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                getDataFromDb();
            }

            @Override
            public void onFailure(Call<List<PayconiqModel>> call, Throwable t) {
                progressBar.setVisibility(View.INVISIBLE);

                getDataFromDb();
            }
        },page,itemCount);
    }


    private void getDataFromDb(){
        RealmResults<PayconiqDao> results=this.dbPresenter.getObjects();
        if(results.size()>0){
            noDataFoundImage.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }else{
            noDataFoundImage.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
        recyclerViewAdapter.setPayconiqModels(results);
        recyclerViewAdapter.notifyItemRangeInserted(previewSize, results.size() - previewSize);
    }

    private int getRequestPage(){
        int page;
        RealmResults<PayconiqDao> results=this.dbPresenter.getObjects();
        page=results.size()/ITEM_COUNT;
        page++;
        return page;
    }

    private PayconiqDao toPayconiqDao(PayconiqModel payconiqModel){
        PayconiqDao payconiqDao=new PayconiqDao();
        payconiqDao.setName(payconiqModel.getName());
        payconiqDao.setId(payconiqModel.getId().longValue());
        return payconiqDao;
    }

    public void onDestroy(){
        super.onDestroy();
        this.dbPresenter.closeDB();
    }
}
