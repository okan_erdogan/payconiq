package com.opcode.payconiq;

import android.app.Application;
import com.opcode.payconiq.di.ApplicationComponent;
import com.opcode.payconiq.di.ApplicationModule;
import com.opcode.payconiq.di.DaggerApplicationComponent;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by TCOERDOGAN on 20.05.2018.
 */

public class PayconiqApplication extends Application {
    private ApplicationComponent applicationComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        RealmConfiguration realmConfiguration =
                new RealmConfiguration.Builder(this)
                        .name("payconiq.db")
                        .schemaVersion(1)
                        .deleteRealmIfMigrationNeeded()
                        .build();

        Realm.setDefaultConfiguration(realmConfiguration);
        initializeInjector();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public void initializeInjector() {
        this.applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }
}
