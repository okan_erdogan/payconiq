package com.opcode.payconiq.presenter;

import com.opcode.payconiq.model.dao.PayconiqDao;
import io.realm.RealmResults;

/**
 * Created by TCOERDOGAN on 20.05.2018.
 */

public interface DbPresenter {
    public void add(PayconiqDao payconiqModel);
    public void closeDB();
    public RealmResults<PayconiqDao> getObjects();
}
