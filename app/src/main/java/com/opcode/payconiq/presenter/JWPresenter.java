package com.opcode.payconiq.presenter;

import com.opcode.payconiq.model.PayconiqModel;
import java.util.List;
import retrofit2.Callback;

/**
 * Created by TCOERDOGAN on 19.05.2018.
 */

public interface JWPresenter {
    public void getRepos(Callback<List<PayconiqModel>> callback,int page,int itemCount);
}
