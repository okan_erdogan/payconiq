package com.opcode.payconiq.presenter;

import android.content.Context;
import com.opcode.payconiq.model.dao.PayconiqDao;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by TCOERDOGAN on 20.05.2018.
 */
public class DbProvider implements DbPresenter {

    private Context context;
    private Realm realm;
    public DbProvider(Context context){
        this.context=context;
    }

    @Override
    public void add(PayconiqDao payconiqModel) {
        this.createDB();
        this.realm.beginTransaction();
        this.realm.copyToRealmOrUpdate(payconiqModel);
        this.realm.commitTransaction();
    }

    @Override
    public void closeDB() {
        this.createDB();
        if(!this.realm.isClosed()){
            this.realm.close();
        }
    }

    @Override
    public RealmResults<PayconiqDao> getObjects() {
        this.createDB();


        if(realm!=null){
            return this.realm.where(PayconiqDao.class).findAll();
        }
        return null;
    }

    private void createDB(){
        if (this.realm == null || this.realm.isClosed()) {
            this.realm = Realm.getDefaultInstance();
        }
    }
}
