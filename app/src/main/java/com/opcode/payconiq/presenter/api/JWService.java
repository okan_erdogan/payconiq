package com.opcode.payconiq.presenter.api;
import com.opcode.payconiq.model.PayconiqModel;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by TCOERDOGAN on 19.05.2018.
 */

public interface JWService {
    @GET("repos?")
    Call<List<PayconiqModel>> getRepos(@Query("page") int page,@Query("per_page") int itemCount);
}
