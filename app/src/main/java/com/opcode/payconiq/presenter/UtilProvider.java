package com.opcode.payconiq.presenter;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by TCOERDOGAN on 20.05.2018.
 */

public class UtilProvider implements UtilPresenter {
    @Override
    public boolean checkInternetConnection(Context context) {
        boolean isConnected = false;

        if(PackageManager.PERMISSION_GRANTED== context.checkCallingOrSelfPermission(Manifest.permission.ACCESS_NETWORK_STATE)){
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
            isConnected = (networkInfo != null && networkInfo.isConnectedOrConnecting());
        }
        return isConnected;
    }


}
