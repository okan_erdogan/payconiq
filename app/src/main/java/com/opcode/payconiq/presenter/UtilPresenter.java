package com.opcode.payconiq.presenter;

import android.content.Context;

/**
 * Created by TCOERDOGAN on 20.05.2018.
 */

public interface UtilPresenter {
    public boolean checkInternetConnection(Context context);
}
