package com.opcode.payconiq.presenter;

import com.opcode.payconiq.model.PayconiqModel;
import com.opcode.payconiq.presenter.api.JWApi;
import com.opcode.payconiq.presenter.api.JWService;

import java.util.List;

import retrofit2.Callback;

/**
 * Created by TCOERDOGAN on 19.05.2018.
 */

public class JWProvider implements JWPresenter{

    @Override
    public void getRepos(Callback<List<PayconiqModel>> callback,int page,int itemCount) {
        JWApi.getClient().create(JWService.class).getRepos(page,itemCount).enqueue(callback);
    }
}
